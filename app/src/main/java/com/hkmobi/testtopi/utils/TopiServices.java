package com.hkmobi.testtopi.utils;

import com.hkmobi.testtopi.model.ResponseRepositorios;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by henrique on 02/02/17.
 */
public interface TopiServices {

    @GET("/search/repositories?q=language:Java&sort=stars&page=1")
    public Call<ResponseRepositorios> getRepositorios();

}
