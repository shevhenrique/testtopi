package com.hkmobi.testtopi.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by henrique on 08/09/17.
 */

public class Proprietario {

    @SerializedName("login")
    private String login;

    @SerializedName("avatar_url")
    private String avatar_url;

    public String getLogin() {
        return login;
    }

    public Proprietario() {
    }

    public Proprietario(String login, String avatar_url) {
        this.login = login;
        this.avatar_url = avatar_url;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public boolean usuarioTemFoto(){
        if(!avatar_url.isEmpty()){
            return true;
        }else{
            return false;
        }

    }
}
