package com.hkmobi.testtopi.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by henrique on 08/09/17.
 */

public class Repositorio {

    @SerializedName("name")
    private String name;

    @SerializedName("full_name")
    private String full_name;

    @SerializedName("description")
    private String description;

    @SerializedName("watchers")
    private Integer watchers;

    @SerializedName("forks_count")
    private Integer forks_count;

    @SerializedName("owner")
    private Proprietario owner;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWatchers() {
        return watchers;
    }

    public void setWatchers(Integer watchers) {
        this.watchers = watchers;
    }

    public Integer getForks_count() {
        return forks_count;
    }

    public void setForks_count(Integer forks_count) {
        this.forks_count = forks_count;
    }

    public Proprietario getOwner() {
        return owner;
    }

    public void setOwner(Proprietario owner) {
        this.owner = owner;
    }
}
