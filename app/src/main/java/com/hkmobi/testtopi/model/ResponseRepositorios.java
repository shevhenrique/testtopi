package com.hkmobi.testtopi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by henrique on 08/09/17.
 */

public class ResponseRepositorios {

    @SerializedName("items")
    private List<Repositorio> items;

    public ResponseRepositorios(List<Repositorio> items) {
        this.items = items;
    }

    public ResponseRepositorios() {
    }

    public List<Repositorio> getItems() {
        return items;
    }

    public void setItems(List<Repositorio> items) {
        this.items = items;
    }

    public boolean verificarListaVazia(){
        return items.size() == 0;
    }
}
