package com.hkmobi.testtopi.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.hkmobi.testtopi.R;
import com.hkmobi.testtopi.adapter.RepositorioAdapter;
import com.hkmobi.testtopi.model.Repositorio;
import com.hkmobi.testtopi.model.ResponseRepositorios;
import com.hkmobi.testtopi.utils.ProgressBarHandler;
import com.hkmobi.testtopi.utils.TopiServices;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<Repositorio> repositorios;
    RepositorioAdapter repositorioAdapter;

    ProgressBarHandler progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        repositorios = new ArrayList<Repositorio>();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        repositorioAdapter = new RepositorioAdapter(this, repositorios);
        recyclerView.setAdapter(repositorioAdapter);

        progressBar = new ProgressBarHandler(this);

        getRepositorios();
    }

    public void getRepositorios() {
        progressBar.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TopiServices service = retrofit.create(TopiServices.class);

        Call<ResponseRepositorios> request = service.getRepositorios();

        request.enqueue(new Callback<ResponseRepositorios>() {

            @Override
            public void onResponse(Call<ResponseRepositorios> call, Response<ResponseRepositorios> response) {
                progressBar.hide();
                if (response.isSuccessful()) {
                    for(Repositorio n: response.body().getItems()){
                        repositorios.add(n);
                    }

                    repositorioAdapter.notifyDataSetChanged();
                } else {
                    System.out.println("Erro " + response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseRepositorios> call, Throwable t) {
                progressBar.hide();
            }
        });
    }
}
