package com.hkmobi.testtopi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hkmobi.testtopi.R;
import com.hkmobi.testtopi.model.Repositorio;

import java.util.List;

public class RepositorioAdapter extends RecyclerView.Adapter<RepositorioAdapter.MyViewHolder> {

    private List<Repositorio> repositorios;
    private LayoutInflater mLayoutInflater;
    private Context context;

    public RepositorioAdapter(Context context, List<Repositorio> repositorios){
        this.context = context;
        this.repositorios = repositorios;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = mLayoutInflater.inflate(R.layout.item_repositorio, viewGroup,false);

        MyViewHolder mvh = new MyViewHolder(v);

        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {

        final Repositorio repositorio  = repositorios.get(position);

        myViewHolder.imagemUsuario.setImageURI(repositorio.getOwner().getAvatar_url());
        myViewHolder.nomeUsuario.setText(repositorio.getOwner().getLogin());
        myViewHolder.nomeRepositorio.setText(repositorio.getName());
        myViewHolder.descricaoRepositorio.setText(repositorio.getDescription());
        myViewHolder.indicador1.setText(repositorio.getForks_count().toString());
        myViewHolder.indicador2.setText(repositorio.getWatchers().toString());

    }

    @Override
    public int getItemCount() {
        return repositorios.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public SimpleDraweeView imagemUsuario;
        public TextView nomeRepositorio;
        public TextView descricaoRepositorio;
        public TextView indicador1;
        public TextView indicador2;
        public TextView sobreNomeUsuario;
        public TextView nomeUsuario;

        public MyViewHolder(View itemView) {
            super(itemView);

            imagemUsuario = (SimpleDraweeView) itemView.findViewById(R.id.imagemUsuario);
            nomeRepositorio = (TextView) itemView.findViewById(R.id.nomeRepositorio);
            descricaoRepositorio = (TextView) itemView.findViewById(R.id.descricaoRepositorio);
            indicador1 = (TextView) itemView.findViewById(R.id.indicador1);
            indicador2 = (TextView) itemView.findViewById(R.id.indicador2);
            sobreNomeUsuario = (TextView) itemView.findViewById(R.id.sobreNomeUsuario);
            nomeUsuario = (TextView) itemView.findViewById(R.id.nomeUsuario);

        }
    }
}
