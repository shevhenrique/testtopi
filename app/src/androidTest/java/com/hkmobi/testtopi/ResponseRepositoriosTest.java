package com.hkmobi.testtopi;

import com.hkmobi.testtopi.model.Proprietario;
import com.hkmobi.testtopi.model.Repositorio;
import com.hkmobi.testtopi.model.ResponseRepositorios;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by henrique on 09/09/17.
 */

public class ResponseRepositoriosTest{

    @Test
    public void verificarSeTemRepositorio(){
        List<Repositorio> repositorios = new ArrayList<>();
        ResponseRepositorios rr = new ResponseRepositorios(repositorios);

        boolean resultado = rr.verificarListaVazia();

        Assert.assertTrue(resultado);
    }

    @Test
    public void verificarSeUsuarioTemFoto(){
        Proprietario p = new Proprietario("Usuario", "");
        boolean resultado = p.usuarioTemFoto();
        Assert.assertFalse(resultado);
    }


}
